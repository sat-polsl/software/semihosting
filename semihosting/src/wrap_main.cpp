#include "semihosting.h"

/*
 * Semihosting library adds option '--wrap=main' to linker.
 * During linking that causes replacement of every symbol 'main' with '__wrap_main'
 * and every symbol '__real_main' with 'main'.
 *
 * Assuming that '__real_main' is called inside '__wrap_main' calling 'main()' will become:
 *
 * __wrap_main() {
 *     main();
 * }
 *
 * Using this allows to insert code, needed to setup and exit semihosting,
 * before and after main() is called without changing the code
 * where main() is actually called (libopencm3 reset_handler() in vectors.c) and allow this library
 * user to write application simply starting with main() function.
 *
 * Before calling main() command line arguments, that will be passed to main(), are collected and
 * output streams are configured to be piped to host streams.
 *
 * After calling main() coverage is dumped to files if library is build with ENABLE_COVERAGE compile
 * definition and application is terminated and exit status is passed to host.
 */

extern "C" int __real_main(int argc, char **argv);

extern "C" void __wrap_main() {
  auto [argv, argc] = semihosting::get_arguments();

  semihosting::initialize_stdout();

  int r = __real_main(argc, argv);

  semihosting::coverage();

  semihosting::exit(r);
}

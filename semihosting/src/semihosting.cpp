#include "semihosting.h"
#include <cstdint>
#include <cstdio>
#include <ctype.h>

extern "C" void initialise_monitor_handles(void);

#ifdef ENABLE_COVERAGE
extern "C" void __gcov_dump();
#endif

namespace semihosting {

  constexpr std::size_t args_buffer_array_size = 1024;
  constexpr std::size_t argv_buffer_array_size = 10;

  struct command_line_block {
    char *command_line;
    int size;
  };

  arguments get_arguments() {
    static char args_buf[args_buffer_array_size];
    static char *argv_buf[argv_buffer_array_size];

    int argc = 0;
    int is_an_argument = 0;

    command_line_block command_block{.command_line = args_buf, .size = sizeof(args_buf) - 1};

    int result = do_AngelSWI(AngelSWI_Reason_GetCmdLine, &command_block);
    if (result == 0) {
      args_buf[args_buffer_array_size - 1] = '\0';

      char *p = command_block.command_line;

      int delim = '\0';
      int ch;

      while ((ch = *p) != '\0') {
        if (is_an_argument == 0) {
          if (!isblank(ch)) {
            if (argc >= (int) ((sizeof(argv_buf) / sizeof(argv_buf[0])) - 1))
              break;

            if (ch == '"' || ch == '\'') {
              delim = ch;
              ++p;
              ch = *p;
            }
            argv_buf[argc++] = p;
            is_an_argument = 1;
          }
        } else if (delim != '\0') {
          if (ch == delim) {
            delim = '\0';
            *p = '\0';
            is_an_argument = 0;
          }
        } else if (isblank(ch)) {
          delim = '\0';
          *p = '\0';
          is_an_argument = 0;
        }
        ++p;
      }
    } else {
      std::printf("Get command line error\n");
      semihosting::exit(1);
    }

    if (argc == 0) {
      args_buf[0] = '\0';
      argv_buf[0] = &args_buf[0];
      ++argc;
    }

    argv_buf[argc] = nullptr;

    return {&argv_buf[0], argc};
  }

  void exit(int status) {
    do_AngelSWI(AngelSWI_Reason_ReportException,
                (void *) (status == 0 ? ADP_Stopped_ApplicationExit : ADP_Stopped_RunTimeError));
    for (;;) {}
  }

  void coverage() {
#ifdef ENABLE_COVERAGE
    __gcov_dump();
#endif
  }

  void initialize_stdout() { initialise_monitor_handles(); }
} // namespace semihosting

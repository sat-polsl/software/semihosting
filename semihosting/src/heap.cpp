#include <cstdint>
#include <cerrno>
#include <sys/reent.h>

extern std::uint8_t _heap, _eheap;

extern "C" void *_sbrk_r(struct _reent *ptr, std::ptrdiff_t incr) {
  static std::uint8_t *heap_end = &_heap;
  std::uint8_t *prev_heap_end = heap_end;

  if (heap_end + incr > &_eheap) {
    ptr->_errno = ENOMEM;
    return reinterpret_cast<void *>(-1);
  }

  heap_end += incr;
  return reinterpret_cast<void *>(prev_heap_end);
}

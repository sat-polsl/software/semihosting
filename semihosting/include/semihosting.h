#pragma once

#define ARM_RDI_MONITOR

#include "swi.h"

namespace semihosting {

/**
 * @defgroup semihosting Semihosting
 * @{
 */

/**
 * @brief Struct holding pointer to command line arguments buffer and size of it.
 */
  struct arguments {
    char **values;
    int number;
  };

/**
 * @brief Returns command line arguments passed by host.
 * @return arguments
 */
  arguments get_arguments();

/**
 * @brief Terminates application execution and passes exic status to host.
 * @param status status returned by application. 0 value indicates success, non-zero value indicates
 * failure.
 */
  void exit(int status);

/**
 * @brief Writes coverage results to host filesystem.
 */
  void coverage();

/**
 * @brief Initializes output streams used by semihosting calls.
 */
  void initialize_stdout();

/** @} */

} // namespace semihosting

#!/bin/bash
QEMU=$1
CPU=$2
ELF=$3

shift 3

if [ "$*" = "" ]
then
  append=""
else
  append="-append"
fi

$QEMU -machine virt_cortex_m,flash_kb=20480 -cpu $CPU -nographic -monitor null -serial stdio -kernel $ELF -semihosting -semihosting-config enable=on,target=native $append "$*"


@echo off
set qemu=%1
set cpu=%2
set elf=%3

for /F "tokens=3,* delims= " %%a in ("%*") do set args=%%b

if "%args%" == "" (
  set append
) else (
  set append=-append
)

%qemu% -machine virt_cortex_m,flash_kb=20480 -cpu %cpu% -nographic -monitor null -serial stdio -kernel %elf% -semihosting -semihosting-config enable=on,target=native %append% "%args%"


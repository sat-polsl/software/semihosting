[![Version](https://img.shields.io/badge/dynamic/json?color=blue&label=Version&query=%24%5B%3A1%5D.name&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F34809163%2Frepository%2Ftags)](https://gitlab.com/sat-polsl/software/packages/semihosting/-/tags)
# Telemetry

# Semihosting

Semihosting library provides capability of ARM semihosting under QEMU executables 
that link to this library.

Executable build with this library will:
1. Get command line arguments passed to QEMU
2. Initialize stdout, stdin, stderr streams
3. Call user provided `int main(int, char**)` with command line arguments
4. Flush gcov output if executable is build with `ENABLE_COVERAGE` compile definition
5. Exit QEMU with status returned by user provided `int main(int, char**)`.

## Usage

1. Provide executable with defined function `int main(int, char**)` that will be entry point to executable.

2. Link this library with CMake:
```cmake
target_link_libraries(${TARGET} PRIVATE semihosting)
```

3. Build
4. Run under QEMU: `tools/run_qemu.sh <qemu> <cpu> <executable> [command line arguments]`
    Supported CPUs: `cortex-m0`, `cortex-m3`, `cortex-m4`, `cortex-m7`



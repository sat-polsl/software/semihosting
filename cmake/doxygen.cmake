find_package(Doxygen)

set(INPUT
    ${CMAKE_SOURCE_DIR}/semihosting
    ${CMAKE_SOURCE_DIR}/README.md
    )

set(DOXYGEN_FILE_PATTERNS
    *.h
    *.cpp
    )

set(DOXYGEN_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/docs")
set(DOXYGEN_USE_MDFILE_AS_MAINPAGE ${CMAKE_SOURCE_DIR}/README.md)

doxygen_add_docs(docs ${INPUT} WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})
